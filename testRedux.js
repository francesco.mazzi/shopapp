const store = require("./src/features/store");
const shopActions = require("./src/features/shop/shopSlice").shopActions;
const fetchUsers = require("./src/features/shop/userSlice").fetchUsers;

console.log("stato iniziale");
console.log(store.getState());
console.log("stato aggiornato");
const unsubscribe = store.subscribe(() => {
  store.getState();
});

store.dispatch(fetchUsers());
// store.dispatch(shopActions.order());
// store.dispatch(shopActions.order());
// store.dispatch(shopActions.restock(10));

// unsubscribe();
