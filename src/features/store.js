import { configureStore } from "@reduxjs/toolkit";
// const reduxLogger = require("redux-logger");
import shopReducer from "./shop/shopSlice";
import productReducer from "./product/productSlice";

// const logger = reduxLogger.createLogger();

const store = configureStore({
  reducer: {
    shop: shopReducer,
    product: productReducer,
  },
  //   middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});

export default store;
