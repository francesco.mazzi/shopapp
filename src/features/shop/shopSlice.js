import { createSlice } from "@reduxjs/toolkit";

//COSEDA IMPLEMENTARE (precisione in base ID)
//1. Fare are in modo che numOfproducts sia unica per ogni prodotto e non totale (forse centra il map che ho fatto su product card)
//2. Fare in modo che deselezioni solo un elemento specifico (e non che se elimini un prodotto si elimina per tutti)
const initialState = {
  numOfproducts: 100,
  total: 0,
};

const shopSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    order: (state) => {
      state.numOfproducts--;
      state.total++;
    },
    restock: (state, action) => {
      state.numOfproducts += action.payload;
      state.total--;
    },
    reset: (state) => {
      state.numOfproducts = 0;
    },
  },
});

export default shopSlice.reducer;
export const { order, restock, reset } = shopSlice.actions;
