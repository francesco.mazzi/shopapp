import ProductList from "./components/Shop";
import Header from "./components/Header";

function App() {
  return (
    <div>
      <>
        <Header></Header>
        <ProductList></ProductList>
      </>
    </div>
  );
}

export default App;
