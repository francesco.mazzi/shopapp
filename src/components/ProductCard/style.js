import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  // productContainer: {
  //   marginTop: "30px",
  // },
  imgContainer: {
    "MuiBox-root": {
      backgroundColor: "#FF8E53",
      height: "20rem",
      width: "20rem",
    },
  },
  title: {
    color: "FF8E53",
    paddingTop: "20px",
    // textAlign: "center",
    fontSize: "3rem",
  },
  //   buttonAdd: {
  //     ...theme.button,
  //   },
});

export default useStyles;
