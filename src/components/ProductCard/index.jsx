import { makeStyles, Typography } from "@mui/material";
import useStyles from "./style";
import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";

//Redux
import { useSelector, useDispatch } from "react-redux";
import { order, reset, restock } from "../../features/shop/shopSlice";
import { fetchProducts } from "../../features/product/productSlice";
import { useState, useEffect } from "react";

const Productview = () => {
  const classes = useStyles();

  const numOfProducts = useSelector((state) => state.shop.numOfproducts);
  const dispatch = useDispatch();

  //Valore numerico restock personalizzato
  const [value, setValue] = useState(0);
  const addValue = Number(value) || " ";

  //Fetch()
  const product = useSelector((state) => state.product);
  console.log(product.products);
  //restuirà:
  //products.title
  //products.price
  //products.id
  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  return (
    <>
      {product.loading && <div>Caricamento...</div>}
      {!product.loading && product.error ? (
        <div>Error... {product.error}</div>
      ) : null}
      {product.products.map((product) => (
        <Card sx={{ maxWidth: 300 }} style={{ margin: "1.1rem" }}>
          <CardMedia
            component="img"
            height="140"
            image={product.image}
            alt="green iguana"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {product.title}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Disponibili ancora {numOfProducts}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Prezzo: {product.price} €
            </Typography>
          </CardContent>
          <CardActions>
            {/* <TextField
              id="standard-basic"
              label="Standard"
              value={addValue}
              variant="standard"
              onChange={(e) => setValue(parseInt(e.target.value))}
            /> */}

            <Button
              id={product.id}
              size="small"
              className={classes.buttonAdd}
              onClick={() => {
                dispatch(order());
                dispatch(fetchProducts());
              }}
            >
              Ordina ora
            </Button>
            <Button
              id={product.id}
              size="small"
              className={classes.buttonAdd}
              onClick={() => {
                dispatch(restock(value));
                dispatch(fetchProducts());
              }}
            >
              Elimina
            </Button>
            {/* <Button
            size="small"
            className={classes.buttonAdd}
            onClick={() => {
              dispatch(reset(value));
              dispatch(fetchProducts());
            }}
          >
            Azzera
          </Button> */}
          </CardActions>
        </Card>
      ))}
    </>
  );
};
export default Productview;
