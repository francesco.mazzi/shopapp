import Productview from "../ProductCard";
import Stack from "@mui/material/Stack";
import Container from "@mui/material/Container";
import { makeStyles, Typography } from "@mui/material";

import { order, restock } from "../../features/shop/shopSlice";
import { useSelector, useDispatch } from "react-redux";
import { fetchProducts } from "../../features/product/productSlice";

const ProductList = () => {
  const totalProducts = useSelector((state) => state.shop.total);

  return (
    <div>
      <Stack>
        <Typography gutterBottom variant="h5" component="div">
          Numero di articoli ordinati: {totalProducts}
        </Typography>
        <Container
          maxWidth="xl"
          style={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: "row",
            justifyContent: "space-around",
            alignItems: "flex-start",
          }}
        >
          <Productview></Productview>
        </Container>
      </Stack>
    </div>
  );
};
export default ProductList;
